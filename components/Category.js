import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const {width, height} = Dimensions.get('screen');

const Category = ({itemData, onPress}) => {
  return (
      <TouchableOpacity  onPress={onPress}>
        <View style={styles.card}>
          <View style={styles.cardImgWrapper}>
            <Image
              source={itemData.image}
              resizeMode="cover"
              style={styles.cardImg}
            />
          </View>
          <View style={styles.cardInfo}>
            <Text style={styles.cardTitle}>{itemData.title}</Text>
            {/* <Text numberOfLines={2} style={styles.cardDetails}>
              {itemData.description}
            </Text> */}
          </View>  
        </View>
    </TouchableOpacity>
  );
};

export default Category;

const styles = StyleSheet.create({
  card: {
    height: 150,
    width: width / 2.5,
    marginVertical: 10,
    marginHorizontal: 10,
    flexDirection: 'column',
    shadowColor: '#999',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  cardImgWrapper: {
    flex: 6,
  },
  cardImg: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 8,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
  },
  cardInfo: {
    flex: 1,
    padding: 10,
    borderColor: '#ccc',
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
    backgroundColor: '#fff',
  },
  cardTitle: {
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  cardDetails: {
    fontSize: 12,
    color: '#444',
  },

  categoryContainer: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 5,
    marginBottom: 12,
  },
  // categoryBtn: {
  //   flex: 1,
  //   width: '45%',
  //   marginHorizontal: 0,
  //   alignSelf: 'center',
  // },
});
