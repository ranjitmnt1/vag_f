import React, {useState} from 'react';

import {
  View,
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {
  WHITE,
  TEXT_SUBTITLE,
  BLACK,
  GREEN,
  TEXT_TITLE,
  APP_THEME,
  GRAY,
} from '../utils/colors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import InputSpinner from 'react-native-input-spinner';

const Product = ({itemData, onPress}) => {
  const [AddCartIndex, setAddCartIndex] = useState('');

  async function RemoveFovItem(item, index) {
    console.log('item ', item);
    // if (auth) {
    //   var params = {
    //     CustomerID: userData.id,
    //     MenuID: item.masterMenuId,
    //     FavID: item.favID,
    //     IsFavStatus: false,
    //   }
    //   // setCategoryListData(cartArray)
    //   const data = await apiCall
    //     ('POST', ENDPOINTS.AddUpdateFavFood, params);

    //   if (data.data.statusCode === 200) {
    //     setAddCartIndex(null)
    //     getCategoryListAfterAdd(userData)
    //     Toast.show(data.data.message)
    //     // setCategoryListData(cartArray)
    //   } else {
    //     Toast.show(data.data.message)
    //   }
    // } else {
    //   Alert.alert(
    //     "For Favourite",
    //     "you have to login first",
    //     [
    //       {
    //         text: "Cancel",
    //         onPress: () => console.log("Cancel Pressed"),
    //         style: "cancel"
    //       },
    //       {
    //         text: "OK", onPress: async () => {

    //           navigation.navigate('LOGIN')
    //         }
    //       }
    //     ],
    //     { cancelable: false }
    //   );
    // }
  }

  async function AddFovItem(item, index) {
    // if (auth) {
    //   var params = {
    //     CustomerID: userData.id,
    //     MenuID: item.masterMenuId,
    //     FavID: 0,
    //     IsFavStatus: true,
    //   }
    //   // setCategoryListData(cartArray)
    //   const data = await apiCall
    //     ('POST', ENDPOINTS.AddUpdateFavFood, params);
    //   if (data.data.statusCode === 200) {
    //     getCategoryListAfterAdd(userData)
    //     Toast.show(data.data.message)
    //     // setCategoryListData(cartArray)
    //   } else {
    //     Toast.show(data.data.message)
    //   }
    // } else {
    //   Alert.alert(
    //     "For Favourite",
    //     "you have to login first",
    //     [
    //       {
    //         text: "Cancel",
    //         onPress: () => console.log("Cancel Pressed"),
    //         style: "cancel"
    //       },
    //       {
    //         text: "OK", onPress: async () => {
    //           navigation.navigate('LOGIN')
    //         }
    //       }
    //     ],
    //     { cancelable: false }
    //   );
    // }
  }

  async function addToCart(item, index) {
    console.log('cartVal', cartVal);
    // if (auth) {
    //   setAddCartIndex(index)
    //   var params = {
    //     CustomerID: userData.id,
    //     MenuID: item.masterMenuId,
    //     Quantity: 1,
    //     MenuItemBasePrice: item.discountPrice,
    //     MenuitemTotalPrice: item.discountPrice,
    //     IsNonOnionGarlic: item.isNonOnionGarlic,
    //     IsVeg: item.masterFoodTypeId == '2' ? false : true,
    //     MenuTotalPrice: item.discountPrice
    //   }

    //   // setCategoryListData(cartArray)
    //   const data = await apiCall
    //     ('POST', ENDPOINTS.AddCustomerCart, params);
    //   console.log("data1111 ", data);
    //   if (data.data.statusCode === 200) {
    //     // Toast.show(data.data.message)
    //     setCartVal(data.data.cartItem)
    //     // getCategoryItemList(await userDetails())
    //     getCategoryListAfterAdd(await userDetails())
    //   } else {
    //     Toast.show(data.data.message)
    //   }
    // } else {
    //   Alert.alert(
    //     "For order",
    //     "you have to login first",
    //     [
    //       {
    //         text: "Cancel",
    //         onPress: () => console.log("Cancel Pressed"),
    //         style: "cancel"
    //       },
    //       {
    //         text: "OK", onPress: async () => {

    //           navigation.navigate('LOGIN')
    //         }
    //       }
    //     ],
    //     { cancelable: false }
    //   );
    // }
  }

  async function onDecrease(i, item) {
    if (i === 0) {
      // DeleteCustomerCart(item)
    } else {
      item.quantity = i;
      // cartAddCustomer(item)
    }
  }

  const onIncrease = async (i, item) => {
    item.quantity = i;
    // cartAddCustomer(item)
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.card}>
        <View style={styles.cardImgWrapper}>
          <Image
            source={itemData.images[0].img}
            resizeMode="cover"
            style={styles.cardImg}
          />
        </View>
        <View style={styles.cardInfo}>
          <View style={{width: '70%'}}>
            <Text
              description
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                paddingLeft: 10,
                marginLeft: 5,
                marginTop: 5,
              }}
              numberOfLines={2}>
              {itemData.title}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              paddingLeft: 30,
            }}>
            {itemData.isFav == true ? (
              <TouchableOpacity
                onPress={() => RemoveFovItem(itemData)}
                style={{paddingLeft: 5, paddingRight: 10}}>
                <Icon
                  name="heart"
                  color="#FF6347"
                  style={{
                    paddingLeft: 3,
                    // color: {APP_THEME},
                    fontSize: 21,
                    fontWeight: 'bold',
                  }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => AddFovItem(itemData)}
                style={{paddingLeft: 5, paddingRight: 10}}>
                <Icon
                  name="heart-outline"
                  color="#FF6347"
                  style={{
                    paddingLeft: 3,
                    fontSize: 21,
                    fontWeight: 'bold',
                  }}
                />
              </TouchableOpacity>
            )}
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingTop: 5,
            }}>
            <View style={{flexDirection: 'row', paddingBottom: 8}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  padding: 4,
                  marginLeft: 5,
                }}>
                <Text
                  style={{
                    color: '#a9a9a9',
                    fontSize: 15,
                    textDecorationLine: 'line-through',
                    color: '#a5a5a5',
                    textDecorationStyle: 'solid',
                    paddingLeft: 4,
                  }}>
                  <FontAwesome
                    name="rupee"
                    style={{
                      paddingLeft: 3,
                      color: '#a9a9a9',
                      fontSize: 15,
                      fontWeight: 'bold',
                    }}
                  />
                  {itemData.unit[0].salesPrice}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  padding: 5,
                }}>
                <FontAwesome
                  name="rupee"
                  style={{paddingLeft: 3, fontSize: 12}}
                />
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  {itemData.unit[0].cost}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: 40,
              }}>
              {/* {cartVal.find(o => o.masterMenuId === item.masterMenuId) ? */}
              {itemData.cartID.length > 0 ? (
                <InputSpinner
                  colorMax={APP_THEME}
                  colorMin={APP_THEME}
                  colorPress={APP_THEME}
                  min={0}
                  step={1}
                  color={APP_THEME}
                  buttonTextColor={'#ffffff'}
                  value={itemData.unit[0].itemCount}
                  onDecrease={i => onDecrease(i, itemData)}
                  onIncrease={i => onIncrease(i, itemData)}
                  buttonStyle={{
                    height: 30,
                    width: 25,
                    borderRadius: 5,
                    borderTopStartRadius: 5,
                    borderTopEndRadius: 5,
                    // alignItems: 'center',
                  }}
                  inputStyle={{
                    height: 30,
                    color: '#000',
                    fontSize: 15,
                    fontWeight: 'bold',
                    alignItems: 'center',
                    // justifyContent:'center',
                    // lineHeight: 8,
                    padding: 4,
                    // borderTopWidth: 0.8,
                    borderTopColor: {APP_THEME},
                    borderBottomColor: {APP_THEME},
                    // borderBottomWidth: 1,
                  }}
                  style={{
                    width: 80,
                    height: 15,
                    marginRight: 5,
                    marginLeft: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  editable={false}
                />
              ) : (
                <TouchableOpacity
                  onPress={() => addToCart(itemData, index)}
                  // disabled={isLoaderCart}
                  style={{
                    backgroundColor: APP_THEME,
                    marginLeft: 20,
                    height: 30,
                    width: 70,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 5,
                    borderRadius: 5,
                  }}>
                  {/* {index == AddCartIndex && isLoaderCart ? (
                    <ActivityIndicator color="#fff" />
                  ) : (
                    <Text style={{color: '#fff', fontWeight: 'bold'}}>
                      Add +
                    </Text>
                  )} */}
                  <Text style={{color: '#fff', fontWeight: 'bold'}}>Add +</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>

          {/* <View style={{flexDirection: 'row',flexWrap: 'wrap'}}>
            <Text style={styles.rupee}>₹</Text>
            <Text style={styles.salesPrice}>100</Text>
            <Text style={styles.strikeThroughtextStyle}>120</Text>
                <View style={{height: 20 , width: 75, alignItems: 'center', textAlignVertical: 'center',marginTop: 3,marginLeft: 10 ,backgroundColor: '#528deb'}}>
                    <Text style={{fontSize: 12, fontWeight: 'bold', color: '#ffffff'}}>12% OFF</Text>
                </View>
           </View> */}
          {/* <Text numberOfLines={2} style={styles.cardDetails}>{itemData.description}</Text> */}
          {/* <View style={{flexDirection: 'row'}}>
            <View
              style={{
                height: 25,
                width: 70,
                alignItems: 'center',
                textAlignVertical: 'center',
                marginLeft: 10,
                backgroundColor: '#FF6347',
              }}>
              <Text
                style={{fontSize: 14, fontWeight: 'bold', color: '#ffffff'}}>
                Add +
              </Text>
            </View>
          </View> */}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Product;

const styles = StyleSheet.create({
  card: {
    height: 120,
    width: '95%',
    marginLeft: 10,
    marginVertical: 10,
    flexDirection: 'row',
    shadowColor: '#999',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  strikeThroughtextStyle: {
    textDecorationLine: 'line-through',
    fontWeight: 'normal',
    paddingLeft: 10,
    fontSize: 19,
    //line-through is the trick
  },
  cardImgWrapper: {
    flex: 1,
  },
  rupee: {
    fontSize: 19,
    fontWeight: 'bold',
  },
  cardImg: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 8,
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
  },
  cardInfo: {
    flex: 2,
    padding: 0,
    borderColor: '#ccc',
    borderWidth: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderLeftWidth: 0,
    // justifyContent: 'space-around',
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: '#fff',
  },
  salesPrice: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  cardTitle: {
    fontWeight: 'normal',
    fontSize: 18,
    marginTop: 4,
  },
  cardDetails: {
    fontSize: 12,
    color: '#444',
  },
});
