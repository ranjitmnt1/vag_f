import React from 'react';
import {useTheme} from '@react-navigation/native';
import { View,StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image';

const HeaderImage = ({url}) => {
    const theme = useTheme();
    return (
        <View >
            <FastImage
                source={{uri: url}}
                resizeMode="cover"
                style={{...styles.headerImage,  borderColor: theme.APP_THEME }}
            />
        </View>
    );
};

export default HeaderImage;

const styles = StyleSheet.create({
  headerImage :{ 
    height: 175,
    overflow: 'hidden',
    borderRadius: 5
  },
});
