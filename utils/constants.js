import {Platform} from 'react-native';

export default (constants = {
  APP_NAME: 'Spoon Click',
  USER_ID: 'UserId',
  ACCESS_TOKEN: 'AccessToken',
  USER_NAME: 'UserName',
  TIME_FORMAT: 'hh:mm A',
  DATE_TIME_FORMAT: 'DD MMM YYYY  hh:mm A',
  FRIEND: 'FRIEND',
  OWNER: 'OWNER',
  CHAT_ROOM: 'CHAT_ROOM',
  CHAT_LIST: 'CHAT_LIST',
  LAST_SEEN: 'LAST_SEEN',
  SCAN_QR_CODE: 'SCAN_QR_CODE',
  USER_STATUS: 'USER_STATUS',
});
export const BASEURL = 'https://api.spoonclick.com';
