import constants from './constants';
import moment from 'moment';  
// import {Toast} from '../Screens/Login/node_modules/native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {StackActions} from '@react-navigation/native';
// import {NAV_TYPES} from './navTypes';
// import io from 'socket.io-client';

export const getTimeInFormat = time => {
  if (time === '') {
    return '';
  }
  const newTime = moment(time).format(constants.TIME_FORMAT);
  return newTime;
};

export const getDateTimeInFormat = time => {
  if (time === '') {
    return '';
  }
  const newTime = moment(time).format(constants.DATE_TIME_FORMAT);
  return newTime;
};

export const getDateTimeStatusFormat = time => {
  if (time === '') {
    return '';
  }
  return `Last update ${getDateTimeInFormat(time)}`;
};

export const getUniqueId = () => {
  const id = JSON.stringify(Date.now());
  console.log('UNIQUE ID => ', id);
  return id;
};

export const storeLocalData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log('AsyncStorage Error', e);
  }
};

export async function clearLocalData() {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    console.log('AsyncStorage Error', e);
  }
}

export const getLocalData = async key => {
  let value = '';
  try {
    value = await AsyncStorage.getItem(key);
    console.log(`AsyncStorage ${key} => `, value);
  } catch (e) {
    console.log('AsyncStorage Error', e);
  }
  return value;
};
export const userDetails = async key => {
  let value = '';
  try {
    value = await AsyncStorage.getItem('userData');
    // console.log(`AsyncStorage=> `, value);
  } catch (e) {
    console.log('AsyncStorage Error', e);
  }
  return JSON.parse(value);
};

export const getUserType = async item => {
  let userId = await getLocalData(constants.USER_ID);
  if (item.userId === userId) {
    // console.log(
    //   'UserType => ',
    //   constants.OWNER + ' User => ' + item.chat[0].chatName,
    // );
    return constants.OWNER;
  } else if (item.chatId === userId) {
    // console.log(
    //   'UserType => ',
    //   constants.FRIEND + ' User => ' + item.chat[0].userName,
    // );
    return constants.FRIEND;
  }
};

export const getUserLoginState = async () => {
  let userToken;
  userToken = null;
  try {
    userToken = await AsyncStorage.getItem('userToken');
    if (userToken == null) {
      return false;
    } else {
      return true;
    }
  } catch (e) {
    console.log(e);
    return false;
  }
};

export async function logoutUser(navigation) {
  await clearLocalData();
  navigation.dispatch(StackActions.replace('LOGIN'));
  // navigation.navigate(NAV_TYPES.LOGIN);
}

export function navigateStack({navigation, screen, props}) {
  navigation && navigation.navigate(screen, {props});
}
