import React from 'react';

import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HomeScreen from './HomeScreen';
import CategoryScreen from './CategoryScreen';

import SearchStackScreen from './search/SearchStackScreen';
import CartScreen from '../screens/cart/CartScreen';

import MyOrderScreen from '../screens/myOrder/MyOrderScreen';

import MyOrderDetailScreen from '../screens/myOrderDetail/MyOrderDetailScreen';

import AdvertisementScreen from './AdvertisementScreen';
import SubCategoryScreen from './SubCategoryScreen';
import SubCategoryFromHomeScreen from './category/SubCategoryFromHomeScreen';
import DeliveryAddressSetScreen from '../screens/DeliveryAddressSet/DeliveryAddressSetScreen';
import AddressScreen from './address/AddressScreen';

import ProductListScreen from './ProductListScreen';

import NotificationScreen from './NotificationScreen';
import ExploreScreen from './ExploreScreen';
import ProfileScreen from './ProfileScreen';
import MapTestScreen from './MapTestScreen';
import EditProfileScreen from './EditProfileScreen';

import {useTheme, Avatar} from 'react-native-paper';
import {View} from 'react-native-animatable';
import {TouchableOpacity} from 'react-native-gesture-handler';
import CardListScreen from './CardListScreen';
import CardItemDetails from './CardItemDetails';

const HomeStack = createStackNavigator();
const NotificationStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const CategoryStack = createStackNavigator();

// const CartStack = createStackNavigator();

const AdvertisementStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = ({navigation}) => (
  <Tab.Navigator initialRouteName="Home" activeColor="#fff">
    <Tab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: 'Home',
        tabBarColor: '#FF6347',
        tabBarIcon: ({color}) => (
          <Icon name="ios-home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Category"
      component={CategoryStackScreen}
      options={{
        tabBarLabel: 'Category',
        tabBarColor: '#FF6347',
        tabBarIcon: ({color}) => (<Feather name="grid" color={color} size={26} /> ),
      }}
    />
    {/* <Tab.Screen
      name="Cart"
      component={CartStackScreen}
      options={{
        tabBarLabel: 'Cart',
        tabBarColor: '#FF6347',
        tabBarIcon: ({color}) => (
          <Icon name="ios-cart" color={color} size={26} />
        ),
      }}
    /> */}
    <Tab.Screen
      name="SearchMain"
      component={SearchStackScreen}
      options={{
        tabBarLabel: 'Cart',
        tabBarColor: '#FF6347',
        tabBarIcon: ({color}) => (
          <Icon
            name="ios-search"
            color={color}
            size={26}
            onPress={() => {
              navigation.navigate('Search');
            }}
          />
        ),
      }}
    />
    <Tab.Screen
      name="Advertisement"
      component={AdvertisementStackScreen}
      options={{
        tabBarLabel: 'Advertisement',
        tabBarColor: '#295d2b',
        tabBarIcon: ({color}) => (
          <FontAwesome name="buysellads" color={color} size={26} />
        ),
      }}
    />

    <Tab.Screen
      name="Profile"
      component={ProfileStackScreen}
      options={{
        tabBarLabel: 'Profile',
        tabBarColor: '#694fad',
        tabBarIcon: ({color}) => (
          <Icon name="ios-person" color={color} size={26} />
        ),
      }}
    />
    {/* <Tab.Screen
      name="Explore"
      component={ExploreScreen}
      options={{
        tabBarLabel: 'Explore',
        tabBarColor: '#d02860',
        tabBarIcon: ({color}) => (
          <Icon name="ios-aperture" color={color} size={26} />
        ),
      }}
    /> */}
  </Tab.Navigator>
);

export default MainTabScreen;

const HomeStackScreen = ({navigation}) => {
  const {colors} = useTheme();
  return (
    <HomeStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.background,
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'VAG-F',
          headerLeft: () => (
            <View style={{marginLeft: 10}}>
              <Icon.Button
                name="ios-menu"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.openDrawer()}
              />
            </View>
          ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {
                  navigation.navigate('Search');
                }}
              />
              <Icon.Button
                name="ios-cart"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {
                  navigation.navigate('CartScreen');
                }}
              />
              <Icon.Button
                name="notification"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {
                  navigation.navigate('NotificationScreen');
                }}
              />
              {/* 
              <TouchableOpacity
                style={{paddingHorizontal: 10, marginTop: 5}}
                onPress={() => {
                  navigation.navigate('Profile');
                }}>
                <Avatar.Image
                  source={{
                    uri:
                      'https://api.adorable.io/avatars/80/abott@adorable.png',
                  }}
                  size={30}
                />
              </TouchableOpacity> */}
            </View>
          ),
        }}
      />
      <HomeStack.Screen
        name="CardListScreen"
        component={CardListScreen}
        options={({route}) => ({
          title: route.params.title,
          headerBackTitleVisible: false,
        })}
      />
      {/* <HomeStack.Screen
        name="All Category"
        component={CategoryScreen}
        options={({route}) => ({
          title: route.params.title,
          headerBackTitleVisible: false,
        })}
      /> */}

      <HomeStack.Screen
        name="CardItemDetails"
        component={CardItemDetails}
        options={({route}) => ({
          // title: route.params.title,
          headerBackTitleVisible: false,
          headerTitle: false,
          headerTransparent: true,
          headerTintColor: '#fff',
        })}
      />

      <HomeStack.Screen
        name="SubCategoryFromHomeScreen"
        component={SubCategoryFromHomeScreen}
        options={{
          title: 'Category',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {}}
              />
            </View>
          ),
        }}
      />

      <HomeStack.Screen
        name="AddressScreen"
        component={AddressScreen}
        options={{
          title: 'Addresses',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {}}
              />
            </View>
          ),
        }}
      />

      <HomeStack.Screen
        name="DeliveryAddressSetScreen"
        component={DeliveryAddressSetScreen}
        options={{
          title: 'Addresses',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {}}
              />
            </View>
          ),
        }}
      />

      <HomeStack.Screen
        name="NotificationScreen"
        component={NotificationScreen}
        options={{
          title: 'Notification',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {}}
              />
            </View>
          ),
        }}
      />

      <HomeStack.Screen
        name="CartScreen"
        component={CartScreen}
        options={{
          title: 'Cart',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.navigate('search')}
              />
            </View>
          ),
        }}
      />

      <CategoryStack.Screen
        name="ProductListScreen"
        component={ProductListScreen}
        options={({route}) => ({
          title: route.params.title,
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.navigate(SubCategoryScreen)}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.navigate('search')}
              />
            </View>
          ),
        })}
      />
    </HomeStack.Navigator>
  );
};

const CategoryStackScreen = ({navigation}) => {
  const {colors} = useTheme();
  return (
    <CategoryStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.background,
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
      <CategoryStack.Screen
        name="CategoryScreen"
        component={CategoryScreen}
        options={{
          title: 'Category',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.goBack()}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => {}}
              />
            </View>
          ),
        }}
      />
      <CategoryStack.Screen
        name="SubCategoryScreen"
        component={SubCategoryScreen}
        options={({route}) => ({
          title: route.params.title,
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.navigate(CategoryScreen)}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.navigate('search')}
              />
            </View>
          ),
        })}
      />
      <CategoryStack.Screen
        name="ProductListScreen"
        component={ProductListScreen}
        options={({route}) => ({
          title: route.params.title,
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.navigate(SubCategoryScreen)}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.navigate('search')}
              />
            </View>
          ),
        })}
      />
    </CategoryStack.Navigator>
  );
};

// const CartStackScreen = ({navigation}) => {
//   const {colors} = useTheme();
//   return (
//     <CartStack.Navigator
//       screenOptions={{
//         headerStyle: {
//           backgroundColor: '#1f65ff',
//         },
//         headerTintColor: '#fff',
//         headerTitleStyle: {
//           fontWeight: 'bold',
//         },
//       }}>
//       <CartStack.Screen
//         name="Cart"
//         component={CartScreen}
//         options={{
//           headerLeft: () => (
//             <Icon.Button
//               name="ios-menu"
//               size={25}
//               backgroundColor="#1f65ff"
//               onPress={() => navigation.openDrawer()}
//             />
//           ),
//         }}
//       />
//     </CartStack.Navigator>
//   );
// };

// const SearchStackScreen = ({navigation}) => {
//   const {colors} = useTheme();
//   return (
//     <SearchStack.Navigator
//       screenOptions={{
//         headerStyle: {
//           backgroundColor: '#1f65ff',
//         },
//         headerTintColor: '#fff',
//         headerTitleStyle: {
//           fontWeight: 'bold',
//         },
//       }}>
//       <SearchStack.Screen
//         name="Search"
//         component={SearchScreen}
//         options={{
//           headerLeft: () => (
//             <Icon.Button
//               name="ios-menu"
//               size={25}
//               backgroundColor="#1f65ff"
//               onPress={() => navigation.openDrawer()}
//             />
//           ),
//         }}
//       />
//     </SearchStack.Navigator>
//   );
// };

const AdvertisementStackScreen = ({navigation}) => {
  const {colors} = useTheme();
  return (
    <AdvertisementStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#295d2b',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
      <AdvertisementStack.Screen
        name="Advertisement"
        component={AdvertisementScreen}
        options={{
          headerLeft: () => (
            <Icon.Button
              name="ios-menu"
              size={25}
              backgroundColor="#295d2b"
              onPress={() => navigation.openDrawer()}
            />
          ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                backgroundColor='#295d2b'
                onPress={() => {
                  navigation.navigate('Search');
                }}
              />
              <Icon.Button
                name="cart-outline"
                size={25}
                backgroundColor='#295d2b'
                onPress={() => {
                  navigation.navigate('CartScreen');
                }}
              />
              <TouchableOpacity
                style={{paddingHorizontal: 10, marginTop: 5}}
                onPress={() => {
                  navigation.navigate('Profile');
                }}>
                <Avatar.Image
                  source={{
                    uri:
                      'https://api.adorable.io/avatars/80/abott@adorable.png',
                  }}
                  size={30}
                />
              </TouchableOpacity>
            </View>
          ),
        }}
      />
    </AdvertisementStack.Navigator>
  );
};

const ProfileStackScreen = ({navigation}) => {
  const {colors} = useTheme();

  return (
    <ProfileStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.background,
          shadowColor: colors.background, // iOS
          elevation: 0, // Android
        },
        headerTintColor: colors.text,
      }}>
      <ProfileStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          title: '',
          headerLeft: () => (
            <View style={{marginLeft: 10}}>
              <Icon.Button
                name="ios-menu"
                size={25}
                backgroundColor={colors.background}
                color={colors.text}
                onPress={() => navigation.openDrawer()}
              />
            </View>
          ),
          headerRight: () => (
            <View style={{marginRight: 10}}>
              <MaterialCommunityIcons.Button
                name="account-edit"
                size={25}
                backgroundColor={colors.background}
                color={colors.text}
                onPress={() => navigation.navigate('EditProfile')}
              />
            </View>
          ),
        }}
      />
      <ProfileStack.Screen
        name="EditProfile"
        options={{
          title: 'Edit Profile',
        }}
        component={EditProfileScreen}
      />

      <ProfileStack.Screen
        name="MyOrderScreen"
        component={MyOrderScreen}
        options={({route}) => ({
          title: 'Orders',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.navigate(SubCategoryScreen)}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.navigate('MyOrderScreen')}
              />
            </View>
          ),
        })}
      />

      <ProfileStack.Screen
        name="MyOrderDetailScreen"
        component={MyOrderDetailScreen}
        options={({route}) => ({
          title: 'Orders Detail',
          // headerLeft: () => (
          //   <Icon.Button
          //     name="ios-arrow-round-back"
          //     size={40}
          //     color={colors.text}
          //     backgroundColor={colors.background}
          //     onPress={() => navigation.navigate(SubCategoryScreen)}
          //   />
          // ),
          headerRight: () => (
            <View style={{flexDirection: 'row', marginRight: 10}}>
              <Icon.Button
                name="ios-search"
                size={25}
                color={colors.text}
                backgroundColor={colors.background}
                onPress={() => navigation.navigate('MyOrderScreen')}
              />
            </View>
          ),
        })}
      />
    </ProfileStack.Navigator>
  );
};
