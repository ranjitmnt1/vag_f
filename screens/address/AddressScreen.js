import React, {useState} from 'react';
import {DEFAULT_STYLES} from '../../utils/styles';

import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
import {useFocusEffect} from '@react-navigation/native';

import Text from '../../components/_Text';

import {
  View,
  StyleSheet,
  AppState,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  Modal,
  Alert,
  StatusBar,
} from 'react-native';
import {
  WHITE,
  TEXT_SUBTITLE,
  BLACK,
  GREEN,
  TEXT_TITLE,
  APP_THEME,
  DISABLED_GRAY,
  GRAY,
} from '../../utils/colors';
import {useTheme} from '@react-navigation/native';

const AddressScreen = ({navigation}) => {
  const theme = useTheme();

  const [modalVisible, setModalVisible] = useState(false);
  const [deliveryAddress, setDeliveryAddress] = useState('');

  function editAddressFun(item) {
    global.addressEdit = item;
    navigation.navigate('DeliveryAddressSetScreen', {type: 'Edit'});
  }

  function addDeliveryAdd(params) {
    // if (auth) {
    global.addressEdit = [];
    navigation.navigate('DeliveryAddressSetScreen', {type: 'add'});
    // } else {
    //   Alert.alert(
    //     "For order",
    //     "you have to login first",
    //     [
    //       {
    //         text: "Cancel",
    //         onPress: () => console.log("Cancel Pressed"),
    //         style: "cancel"
    //       },
    //       {
    //         text: "OK", onPress: async () => {

    //           navigation.navigate('LOGIN')
    //         }
    //       }
    //     ],
    //     { cancelable: false }
    //   );
    // }
  }

  const allDeliveryAddress = [
    {
      Name: 'Sandeep Patidar',
      MobileNo: '9981832936',
      House: '270',
      Area: 'Bagli',
      LandMark: 'Near Ganesh Mandir',
      City: 'Dewas',
      State: 'Madhya Pradesh',
      PinCode: '455227',
      AddressType: 'Home',
      DefaultAddress: true,
      CustomerId: 1,
      AddressID: 1,
    },
    {
      Name: 'Sandeep Patidar',
      MobileNo: '9981832936',
      House: '270',
      Area: 'Bagli',
      LandMark: 'Near Ganesh Mandir',
      City: 'Dewas',
      State: 'Madhya Pradesh',
      PinCode: '455227',
      AddressType: 'Home',
      DefaultAddress: true,
      CustomerId: 1,
      AddressID: 2,
    },
    {
      Name: 'Sandeep Patidar',
      MobileNo: '9981832936',
      House: '270',
      Area: 'Bagli',
      LandMark: 'Near Ganesh Mandir',
      City: 'Dewas',
      State: 'Madhya Pradesh',
      PinCode: '455227',
      AddressType: 'Home',
      DefaultAddress: true,
      CustomerId: 1,
      AddressID: 3,
    },
    {
      Name: 'Sandeep Patidar',
      MobileNo: '9981832936',
      House: '270',
      Area: 'Bagli',
      LandMark: 'Near Ganesh Mandir',
      City: 'Dewas',
      State: 'Madhya Pradesh',
      PinCode: '455227',
      AddressType: 'Home',
      DefaultAddress: true,
      CustomerId: 1,
      AddressID: 4,
    },
  ];
  return (
    <SafeAreaView style={DEFAULT_STYLES.container}>
      <StatusBar barStyle={theme.dark ? 'light-content' : 'dark-content'} />
      <View style={DEFAULT_STYLES.container}>
        <View style={{flex: 1}}>
          <ScrollView showsHorizontalScrollIndicator={false}>
            <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
              {allDeliveryAddress &&
                allDeliveryAddress.map((item, Index) => {
                  return (
                    <View
                      style={{
                        // flex: 0.5,
                        width: '100%',
                        // backgroundColor: '#fff',
                        padding: 3,
                        minHeight: 160,
                      }}>
                      <View
                        key={Index}
                        style={{
                          flex: 0.5,
                          backgroundColor: '#fff',
                          borderBottomWidth: 0,
                          borderColor: APP_THEME,
                          shadowColor: GRAY,
                          shadowOpacity: 1,
                          shadowRadius: 1,
                          shadowOffset: {
                            height: 1,
                            width: 0.3,
                          },
                          elevation: 5,
                          borderRadius: 10,
                          margin: 5,
                          padding: 8,
                        }}>
                        <View
                          style={{
                            minHeight: 85,
                            borderBottomWidth: 1,
                            borderBottomColor: '#eaeaea',
                            marginHorizontal: 10,
                          }}>
                          <View style={{}}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: DISABLED_GRAY,
                                fontSize: 18,
                              }}>
                              {/* {item.name} */}
                              Sandeep Patidar
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: DISABLED_GRAY,
                              }}>
                              Guradiya Kalan, Near Ganesh Mandir, 270, Bagli,
                              Dewas, Madhya Pradesh, 455227
                              {/* {item.house} {item.landMark}
                              {', '}
                              {item.city}
                              {', '}
                              {item.state}
                              {', '}
                              {item.pinCode}{' '} */}
                            </Text>
                          </View>
                        </View>
                        <View
                          style={{
                            alignItems: 'center',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingHorizontal: 5,
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <CheckBox
                              value={item.defaultAddress}
                              //   onValueChange={() => defaultAddressFun(item)}
                              tintColors={{true: APP_THEME}}
                            />

                            <Text
                              style={{
                                color: DISABLED_GRAY,
                                fontWeight: 'bold',
                              }}>
                              Set as Default
                            </Text>
                          </View>
                          <TouchableOpacity
                            onPress={() => editAddressFun(item)}
                            style={{justifyContent: 'center', paddingTop: 5}}>
                            <Text style={{fontSize: 14, color: APP_THEME}}>
                              Edit
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() => {
                              setModalVisible(!modalVisible),
                                setDeliveryAddress(
                                  item.name +
                                    ' ' +
                                    item.house +
                                    ' ' +
                                    item.landMark +
                                    ' ' +
                                    item.city +
                                    ' ' +
                                    item.state +
                                    ' ' +
                                    item.pinCode,
                                );
                            }}
                            style={{justifyContent: 'center', paddingTop: 5}}>
                            <Text style={{fontSize: 14, color: APP_THEME}}>
                              Delete
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>
          </ScrollView>
          {!allDeliveryAddress && (
            <View
              style={{
                height: '70%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ImageBackground
                source={require('../../assets/location.png')}
                style={{width: 110, height: 110}}
              />
              <Text style={{fontWeight: 'bold', fontSize: 18, marginTop: 10}}>
                No Delivery Address Available
              </Text>
            </View>
          )}
          <TouchableOpacity
            onPress={() => addDeliveryAdd()}
            style={{
              height: 45,
              backgroundColor: APP_THEME,
              margin: 12,
              borderRadius: 8,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>
              Add New Delivery Address
            </Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '90%',
                // height: 200,
                backgroundColor: '#fff',
                borderWidth: 1,
                borderRadius: 18,
                shadowOpacity: 1,
                shadowRadius: 1,
                shadowOffset: {
                  height: 1,
                  width: 0.3,
                },
                elevation: 10,
                paddingVertical: 10,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '90%',
                  marginLeft: 15,
                }}>
                <Text style={{fontSize: 20, textAlign: 'center'}}>
                  Are you sure you want to delete this delivery address{' '}
                </Text>
              </View>
              <View
                style={{backgroundColor: '#cccccc', margin: 15, padding: 10}}>
                <Text>{deliveryAddress}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  paddingBottom: 10,
                }}>
                <TouchableOpacity
                  onPress={() => setModalVisible(!modalVisible)}
                  style={{
                    width: 70,
                    height: 35,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                    backgroundColor: APP_THEME,
                  }}>
                  <Text style={{color: '#fff'}}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => setModalVisible(!modalVisible)}
                  style={{
                    width: 70,
                    height: 35,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                    backgroundColor: APP_THEME,
                  }}>
                  <Text style={{color: '#fff'}}>No</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

export default AddressScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
