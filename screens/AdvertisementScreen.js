import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const AdvertisementScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text>Card Screen</Text>
      <Button title="Click here" onPress={() => alert('Button Clicled!!')} />
    </View>
  );
};

export default AdvertisementScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#8fcbbc',
  },
});
