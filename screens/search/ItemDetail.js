import React from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    ScrollView,
    Image,
    Platform,
    StatusBar
} from "react-native";

import {
    Text,
    Title,
    Subheading,
    Button,
    Divider,
    Caption,
    Chip,
    List,
    Snackbar
} from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useTheme } from '@react-navigation/native';
import Swiper from 'react-native-swiper';
const ItemDetailsScreen = ({ navigation, route }) => {
    // const itemData = route.params.itemData;
    var itemData = {
        id: 1,
        title: 'Society Tea',
        description: 'So Delicious® Dairy Free Coconut Milk Yogurt Alternative Vanilla. Made with organic coconut. Net Wt. 5.3 oz (150 g). Certified Vegan. Certified Gluten-Free®.',
        category: 'Tea',
        brand: 'Society Tea',
        expDate: '05/14/2017',
        mfdDate: '05/18/2017',
        size: '5.3 oz cup',
        unit: [
            {
                id: 1,
                packing: 1,
                type: 'kg',
                salesPrice: 20,
                cost: 10,
                stock: '15',
            },
            {
                id: 2,
                packing: 500,
                type: 'gram',
                salesPrice: 12,
                cost: 10,
                stock: '15',
            },
            {
                id: 3,
                packing: 250,
                type: 'gram',
                salesPrice: 12,
                cost: 10,
                stock: '15',
            },
        ],
        images: [{ img: require('../../assets/banners/food-banner1.jpg') }, { img: require('../../assets/banners/food-banner2.jpg')  },{ img : require('../../assets/banners/food-banner3.jpg')}],
        suggestion: ['Keep refrigerated.'],
        allegations: [
            'Contains coconut.',
            'Produced in a facility that also processes other tree nuts and soy.',
            'We apply strict quality control measures to prevent contamination by undeclared food allergens',
        ],
    };
    const theme = useTheme();

    const initialMapState = {
        selectedItem: itemData,
        visible : 'Hide'
    };
    const onToggleSnackBar = (action) => {
        setData({
            ...state,
            visible : action
        })
    }

    const onDismissSnackBar = (action) => {
        setData({
            ...state,
            visible : action
        })
    }
    const showAllImages = () => {
       console.log("Double clicked ")
    }
    

    const [state, setData] = React.useState(initialMapState);

    return (
        <View>
            {/* <StatusBar barStyle={theme.dark ? 'light-content' : 'dark-content'} /> */}
            <View style={styles.detailContainer} >
                <ScrollView >
                    <Title    style={{ marginLeft: 5 }}> { state.selectedItem.title} </Title>
                    <View style={styles.sliderContainer} >
                        <Swiper
                            autoplay
                            horizontal={true}
                            bounces ={ true }
                          
                            height={200} activeDotColor="#FF6347">
                            {  state.selectedItem &&  state.selectedItem.images.map((item,index) => (
                                    <View  key={index} style={styles.slide}>
                                        <Image
                                            source={ item.img }
                                            resizeMode="cover"
                                            style={styles.sliderImage}
                                            bounces={true}
                                            onPress={() =>showAllImages()}
                                        />
                                    </View>
                                 
                            ))}    
                            
                        </Swiper>
                    </View>
                    
                    <View style={{ marginTop : 15, marginLeft : 5}}>
                        <Caption >{ state.selectedItem.description || ''}</Caption>
                    </View>
                    <View style={styles.priceArea }>
                        <View >
                            <Text style={styles.productDesc}> MRP : ₹300</Text> 
                            <Text style={{fontSize: 18 , fontWeight : "bold"}}> SP : ₹290</Text> 
                            <Text style={styles.productDesc}> (inclusive all taxes)) </Text>
                        </View>
                        <View >
                            <Button icon="cart" color={'red'} mode="contained" onPress={() => console.log('Pressed')}>
                                Add To Cart
                            </Button>
                        </View>
                    </View>
                    <Divider style={{ marginTop : 15}} />
                    <View style={{ marginTop : 15, marginLeft : 5}}>
                        <Subheading>Unit</Subheading>
                        <View style={{ flexDirection :'row' }}>
                            <Chip selected style={{ marginRight : 5} } onPress={() => console.log('Pressed')}>1L</Chip>
                            <Chip  onPress={() => console.log('Pressed')}>2L</Chip>
                        </View>
                    </View>
                    <Divider  style={{ marginTop : 15}} />
                    <List.Section title="Info">
                        <List.Accordion title="Manufacturer Details" >
                            <List.Item title="komal Enterpricess" />
                        </List.Accordion>
                        <List.Accordion title="Marked By" >
                            <List.Item title="komal Enterpricess , Pune" />
                        </List.Accordion>
                        <List.Accordion title="License" >
                            <List.Item title="115121dsaf213" />
                        </List.Accordion>
                        <List.Accordion title="Disclaimer" >
                            <List.Item title="Every effort is made to maintain accuracy of all
                            information.However actual product packaging and materials mya contain more and /or different information." />
                        </List.Accordion>
                        <List.Accordion title="Country of Origin" >
                            <List.Item title="India" />
                        </List.Accordion>
                        <List.Accordion title="Customer Care Details" >
                            <List.Item title="Customer Care No. 1-908-203-3344" />
                            <List.Item title="Customer Care Mail Id: info@vagf.com" />
                        </List.Accordion>
                        <List.Accordion title="Seller" >
                            <List.Item title="VAG-F PVt.LTD" />
                        </List.Accordion>
                    </List.Section>
                    <Divider  style={{ marginTop : 15}} />  
                    <Button onPress={onToggleSnackBar}>{state.visible ? 'Hide' : 'Show'}</Button>
                    <Snackbar
                        visible={state.visible}
                        onDismiss={onDismissSnackBar}
                        action={{
                        label: 'Undo',
                        onPress: () => {
                            // Do something
                        },
                        }}>
                        Hey there! I'm a Snackbar.
                    </Snackbar>
                </ScrollView>
            </View>
        </View>
    );
};
export default ItemDetailsScreen;

const styles = StyleSheet.create({
    backBtn: {
        marginTop: 35,
        marginLeft: 10
    },
    secTitle: {
        fontWeight: 'bold'
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 8,
    },
    sliderImage: {
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
    },
    detailContainer: {
        paddingTop: 20,
        paddingLeft: 10,
        paddingRight: 10

    },
    sliderContainer: {
        height: 200,
        width: '95%',
        marginTop: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    productDesc: {
        //color: '#70757a',
        fontSize: 16
    }, 
    priceArea : {
        paddingTop: 10,
        flex: 1,
        justifyContent: 'space-between',
        flexDirection : 'row'
    }
});    