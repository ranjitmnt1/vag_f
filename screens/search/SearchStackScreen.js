import React from 'react';

import SearchScreen from "./SearchScreen";
import ItemDetailsScreen from './ItemDetail';
import { createStackNavigator } from '@react-navigation/stack';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import {View} from 'react-native-animatable';
import { useTheme, Avatar } from 'react-native-paper';
//header: () => {}
const RootStack = createStackNavigator();

const SearchStackScreen = ({navigator}) => {
    const {colors} = useTheme();
    return ( 
        <RootStack.Navigator  >
            <RootStack.Screen 
                name="SearchScreen" 
                component={SearchScreen}
                options ={{
                    headerShown: false
                }}/>
            <RootStack.Screen name="ItemDetail" 
            component={ItemDetailsScreen}
                options={{
                    title : "Details"
                }}
            />
        </RootStack.Navigator> 
    )
};  

export default SearchStackScreen;