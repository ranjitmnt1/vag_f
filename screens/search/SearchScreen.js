import React from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  ScrollView,
  Image,
  Platform
} from 'react-native';


import Ionicons from 'react-native-vector-icons/Ionicons';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {HeaderBackButton} from '@react-navigation/stack';
import {Text, TouchableRipple} from 'react-native-paper';

const SearchScreen = ({navigation}) => {
  const theme = useTheme();
  const TotalData = [
    {text: 'Sugar', img: require('../../assets/sugar.jpg')},
    {
      text: 'Oil',
      img: require('../../assets/oil.jpeg'),
    },
    {
      text: 'Atta',
      img: require('../../assets/atta.jpg'),
    },
    {
      text: 'Ghee',
      img: require('../../assets/ghee.jpeg'),
    },
    {
      text: 'Biscuit',
      img: require('../../assets/biscuit.jpeg'),
    },
    {text: 'Sugar', img: require('../../assets/sugar.jpg')},
    {
      text: 'Oil',
      img: require('../../assets/oil.jpeg'),
    },
    {
      text: 'Atta',
      img: require('../../assets/atta.jpg'),
    },
    {
      text: 'Ghee',
      img: require('../../assets/ghee.jpeg'),
    },
    {
      text: 'Biscuit',
      img: require('../../assets/biscuit.jpeg'),
    },
    {text: 'Sugar', img: require('../../assets/sugar.jpg')},
    {
      text: 'Oil',
      img: require('../../assets/oil.jpeg'),
    },
    {
      text: 'Atta',
      img: require('../../assets/atta.jpg'),
    },
    {
      text: 'Ghee',
      img: require('../../assets/ghee.jpeg'),
    },
    {
      text: 'Biscuit',
      img: require('../../assets/biscuit.jpeg'),
    },
    {text: 'Sugar', img: require('../../assets/sugar.jpg')},
    {
      text: 'Oil',
      img: require('../../assets/oil.jpeg'),
    },
    {
      text: 'Atta',
      img: require('../../assets/atta.jpg'),
    },
    {
      text: 'Ghee',
      img: require('../../assets/ghee.jpeg'),
    },
    {
      text: 'Biscuit',
      img: require('../../assets/biscuit.jpeg'),
    },
  ];
  const initialMapState = {
    searchData: TotalData,
  };

  const [state, setData] = React.useState(initialMapState);
  const handleSearchChange = val => {
    if (val && val.length > 0) {
      const foundResult = TotalData.filter(item => {
        return (
          item.text &&
          item.text.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) > -1
        );
      });
      setData({
        ...state,
        searchData: foundResult,
      });
    } else {
      setData({
        ...state,
        searchData: TotalData,
      });
    }
  };
  const handleItemSelect = item => {
    navigation.navigate('ItemDetail', {selectedItem: item});
  };

  return (
    <View>
      <Ionicons
        name="md-arrow-back"
        style={styles.backBtn}
        size={25}
        onPress={() => {
          navigation.goBack();
        }}
      />

      <View style={styles.searchBox}>
        <TextInput
          placeholder="Search here"
          placeholderTextColor="#000"
          autoCapitalize="none"
          style={{flex: 1, padding: 0}}
          onChangeText={val => handleSearchChange(val)}
        />
        <Ionicons name="ios-search" size={20} />
      </View>
      <View style={styles.resultContainer}>
        <ScrollView>
          <Text style={styles.secTitle}> Treading Search </Text>
          {state.searchData.map((item, index) => (
            <TouchableRipple
              key={index}
              onPress={() => {
                handleItemSelect(item);
              }}>
              <View style={styles.menuItem}>
                <Image style={styles.menuItemImg} source={item.img} />
                <Text style={styles.menuItemText}>{item.text}</Text>
              </View>
            </TouchableRipple>
          ))}
        </ScrollView>
      </View>
    </View>
  );
};
export default SearchScreen;

const styles = StyleSheet.create({
  backBtn: {
    marginTop: 35,
    marginLeft: 10,
  },
  secTitle: {
    fontWeight: 'bold',
  },
  searchBox: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '85%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
    left: 50,
  },
  resultContainer: {
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  listItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  menuItemImg: {
    width: 35,
    height: 35,
  },
});
