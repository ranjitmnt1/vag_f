import React from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Avatar, Paragraph, Card, Title } from 'react-native-paper';
import HeaderImage from '../../components/HeaderImage';
import FastImage from 'react-native-fast-image';
const LeftContent = props => <Avatar.Icon {...props} icon="folder" />
const SupportScreen = ({navigation}) => {
    return (
      <View style={styles.container}>
       <View style={{ ...styles.areaDist,flexDirection : 'row',justifyContent :'center'}}>
          <Ionicons
            name="md-arrow-back"
            style={{...styles.backBtn,position:'absolute',left:0}}
            size={25}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text style={styles.headerTitle}>Help And Support</Text>
        </View>
        <ScrollView>
          <HeaderImage style={styles.areaDist} url={'https://unsplash.it/400/400?image=1'}></HeaderImage>
          <View style={{ alignItems : 'center'}}>
            <Card style={{ ...styles.card}}>
              <Card.Content style={{ alignItems : 'center'}}>
                <FastImage
                          source={{uri: 'https://unsplash.it/400/400?image=1'}}
                          resizeMode="cover"
                          style={{
                            ...styles.areaDist,
                            width: 65,
                            height: 65,
                            // borderColor: theme.APP_THEME,
                            overflow: 'hidden',
                            borderRadius: 70 / 2
                          }}
                        />   
                     
                <Title>VISIT OUR OFFICE</Title>
                <Paragraph>Nitron Classic, St. Patrick Town</Paragraph>
                <Paragraph>Hadapsar, Pune - 410013, India</Paragraph>  
              </Card.Content>
            </Card>

            <Card style={{ ...styles.card}}>
              <Card.Content style={{ alignItems : 'center'}}>
                <FastImage
                          source={{uri: 'https://unsplash.it/400/400?image=1'}}
                          resizeMode="cover"
                          style={{
                            ...styles.areaDist,
                            width: 65,
                            height: 65,
                            // borderColor: theme.APP_THEME,
                            overflow: 'hidden',
                            borderRadius: 70 / 2
                          }}
                        />   
                <Title>LET'S TALK</Title>
                <Paragraph>Phone. : 9000000000</Paragraph>
                <Paragraph>Tel. : 8000000000</Paragraph> 
              </Card.Content>
            </Card>

            <Card style={{ ...styles.card}}>
              <Card.Content style={{ alignItems : 'center'}}>
                <FastImage
                          source={{uri: 'https://unsplash.it/400/400?image=1'}}
                          resizeMode="cover"
                          style={{
                            ...styles.areaDist,
                            width: 65,
                            height: 65,
                            // borderColor: theme.APP_THEME,
                            overflow: 'hidden',
                            borderRadius: 70 / 2
                          }}
                        />   
                <Title>E-MAIL US</Title>
                <Paragraph>vegf@gmail.com</Paragraph>
                <Paragraph>vegf1@gmail.com</Paragraph>  
              </Card.Content>
            </Card>
            </View>  
        </ScrollView>
      </View>
    );
};

export default SupportScreen;

const styles = StyleSheet.create({
  container: {
    flex : 1,
    padding : 10
  },
  headerTitle :{ 
    fontSize : 22,
    alignSelf: 'center'
  },
  backBtn : {
  },
  areaDist :{
    marginBottom : 10
  },
  card: {
    marginVertical: 10,
    flexDirection: 'row',
    shadowColor: '#999',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 0,
    elevation: 10,
    width : '90%'
  }
});
