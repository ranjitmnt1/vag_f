import React from 'react';
import {useTheme} from '@react-navigation/native';
import { View, Text,FlatList, StyleSheet ,ScrollView} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Paragraph, Title,Headline, Subheading  } from 'react-native-paper';
import FastImage from 'react-native-fast-image';
import HeaderImage from '../../components/HeaderImage';
import {
  WHITE,
  APP_THEME
} from '../../utils/colors';
import {OurTeam} from '../../model/OurTeam';

const AboutUs = ({navigation}) => {
  const theme = useTheme();
    return (
      <View style={styles.container}>
        <View style={{ ...styles.areaDist,flexDirection : 'row',justifyContent :'center'}}>
          <Ionicons
            name="md-arrow-back"
            style={{...styles.backBtn,position:'absolute',left:0}}
            size={25}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text style={styles.headerTitle}>About Us Screen</Text>
        </View>
        <ScrollView>
          <HeaderImage style={styles.areaDist} url={'https://unsplash.it/400/400?image=1'}></HeaderImage>
        
          <View style={styles.areaDist}>
            <Title>Our Story</Title>
            <Paragraph style={{fontStyle :'italic'}}> "The pain itself is the pain Very, very small. No one prevents our flight from being unencumbered by any of these things which he often chooses."</Paragraph>
            <Paragraph> The pain itself is important to the main adipisicing elite. All we can do is deem it a true, acceptable choice. The pain itself is important to the main adipisicing elite. They don't know any other option or runs away from it. the main adipisic process.It shouldn't be assumed, but how easy is it to carry messages?</Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Mission</Title>
            <Paragraph>   The pain itself is the pain For no pain is the result of a happy softness right?</Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Vision</Title>
            <Paragraph>   The pain itself is important to the main adipisicing elite. We must take to obtain the main, all our pleasures and pains.</Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Out Team</Title>
            <View
              style={{
                marginTop: 5,
                width: '90%',
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              
              <FlatList
                data={OurTeam}
                horizontal={false}
                contentContainerStyle={{alignItems: 'center', padding: 8}}
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => (
                  <View>
                    <View
                      style={{
                        height: 75,
                        width: 75,
                        borderRadius: 100,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: WHITE,
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 4,
                        },
                        shadowOpacity: 0.32,
                        shadowRadius: 5.46,

                        elevation: 1.5,
                        borderColor: APP_THEME,
                        borderWidth: 1,
                        flexDirection: "column",
                        alignSelf: 'center'
                      }}>
                      <FastImage
                        source={{uri: 'https://unsplash.it/400/400?image=1'}}
                        resizeMode="cover"
                        style={{
                          width: 65,
                          height: 65,
                          borderColor: theme.APP_THEME,
                          overflow: 'hidden',
                          borderRadius: 70 / 2
                        }}
                      />
                    </View>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 5
                      }}>
                      <Headline 
                        description
                        numberOfLines={1}
                        style={{
                          backgroundColor: theme.backgroundColor,
                          color: theme.Text,
                        }}>
                        {item.name}
                      </Headline >
                      <Subheading 
                        description
                        numberOfLines={1}
                        style={{
                          backgroundColor: theme.backgroundColor,
                          color: theme.Text,
                        }}>
                        {item.designation}
                      </Subheading>
                    </View> 
                  </View>
                )}
                ItemSeparatorComponent={() => {
                  return (
                    <View
                      style={{
                        // height: "100%",
                        width: 25,
                        backgroundColor: "#000",
                      }}
                    />
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View> 
        </ScrollView>
      </View>  
    );
};

export default AboutUs;

const styles = StyleSheet.create({
  container: {
    flex : 1,
    padding : 10
  },
  headerTitle :{ 
    fontSize : 22,
    alignSelf: 'center'
  },
  backBtn : {
  },
  areaDist :{
    marginBottom : 10
  }

});
