import React from 'react';
import {useTheme} from '@react-navigation/native';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Paragraph, Title} from 'react-native-paper';
import HeaderImage from '../../components/HeaderImage';
const TermsAndConditions = ({navigation}) => {
    const theme = useTheme();
    return (
      <View style={styles.container}>
        <View style={{ ...styles.areaDist,flexDirection : 'row',justifyContent :'center'}}>
          <Ionicons
            name="md-arrow-back"
            style={{...styles.backBtn,position:'absolute',left:0}}
            size={25}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text style={styles.headerTitle}>Terms And conditions</Text>
        </View>
        <ScrollView>
          <HeaderImage style={styles.areaDist} url={'https://unsplash.it/400/400?image=1'}></HeaderImage>

          <View style={styles.areaDist}>
            <Title>Conditions of use</Title>
            <Paragraph>By using this website, you certify that you have read and reviewed this Agreement and that you
              agree to comply with its terms. If you do not want to be bound by the terms of this Agreement,
              you are advised to leave the website accordingly. [name] only grants use and access of this
              website, its products, and its services to those who have accepted its terms.
              </Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Privacy policy</Title>
            <Paragraph>Before you continue using our website, we advise you to read our privacy policy [link to privacy
              policy] regarding our user data collection. It will help you better understand our practices.
              </Paragraph>
          </View>


          <View style={styles.areaDist}>
            <Title>Age restriction</Title>
            <Paragraph>You must be at least 18 (eighteen) years of age before you can use this website. By using this
                website, you warrant that you are at least 18 years of age and you may legally adhere to this
                Agreement. [name] assumes no responsibility for liabilities related to age misrepresentation.
              </Paragraph>
          </View>


          <View style={styles.areaDist}>
            <Title>Intellectual property</Title>
            <Paragraph>You agree that all materials, products, and services provided on this website are the property of
                [name], its affiliates, directors, officers, employees, agents, suppliers, or licensors including all
                copyrights, trade secrets, trademarks, patents, and other intellectual property. You also agree
                that you will not reproduce or redistribute the [name]’s intellectual property in any way,
                including electronic, digital, or new trademark registrations.
                You grant [name] a royalty-free and non-exclusive license to display, use, copy, transmit, and
                broadcast the content you upload and publish. For issues regarding intellectual property claims,
                you should contact the company in order to come to an agreement.

              </Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>User accounts</Title>
            <Paragraph>As a user of this website, you may be asked to register with us and provide private information.
              You are responsible for ensuring the accuracy of this information, and you are responsible for
              maintaining the safety and security of your identifying information. You are also responsible for
              all activities that occur under your account or password.
              If you think there are any possible issues regarding the security of your account on the website,
              inform us immediately so we may address them accordingly.
              We reserve all rights to terminate accounts, edit or remove content and cancel orders at our sole
              discretion.
            </Paragraph>
          </View>


          <View style={styles.areaDist}>
            <Title>Applicable law</Title>
            <Paragraph>By visiting this website, you agree that the laws of the [location], without regard to principles of
                conflict laws, will govern these terms and conditions, or any dispute of any sort that might come
                between [name] and you, or its business partners and associates.

              </Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Disputes</Title>
            <Paragraph>
              Any dispute related in any way to your visit to this website or to products you purchase from us shall be arbitrated by state or federal court [location] and you consent to exclusive jurisdiction and venue of such courts.
            </Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Indemnification</Title>
            <Paragraph>
              You agree to indemnify [name] and its affiliates and hold [name] harmless against legal claims and demands that may arise from your use or misuse of our services. We reserve the right to select our own legal counsel. 
            </Paragraph>
          </View>

          <View style={styles.areaDist}>
            <Title>Limitation on liability</Title>
            <Paragraph>
              [name] is not liable for any damages that may occur to you as a result of your misuse of our website.
            </Paragraph>
            <Paragraph>
              [name] reserves the right to edit, modify, and change this Agreement at any time. We shall let our users know of these changes through electronic mail. This Agreement is an understanding between [name] and the user, and this supersedes and replaces all prior agreements regarding the use of this website.
            </Paragraph>
          </View>
        </ScrollView>  
      </View>
    );
};

export default TermsAndConditions;

const styles = StyleSheet.create({
  container: {
    flex : 1,
    padding : 10
  },
  headerTitle :{ 
    fontSize : 22,
    alignSelf: 'center'
  },
  backBtn : {
  },
  areaDist :{
    marginBottom : 10
  }

});
