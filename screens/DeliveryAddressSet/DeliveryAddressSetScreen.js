import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  AppState,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  CheckBox,
  TextInput,
} from 'react-native';
import {DEFAULT_STYLES} from '../../utils/styles';
import {
  WHITE,
  TEXT_SUBTITLE,
  BLACK,
  GREEN,
  TEXT_TITLE,
  APP_THEME,
  GRAY,
} from '../../utils/colors';
// import Toast from 'react-native-simple-toast';

import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
// import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useFocusEffect} from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';
// import Feather from 'react-native-vector-icons/dist/Feather';
// import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
// import Ionicons from 'react-native-vector-icons/dist/Ionicons';
// import AntDesign from 'react-native-vector-icons/dist/AntDesign';
// import SimpleLineIcons from 'react-native-vector-icons/dist/SimpleLineIcons';
// import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import _Divider from '../../components/_Divider';
import Text from '../../components/_Text';
// import { apiCall, setDefaultHeader } from '../../utils/httpClient';
// import ENDPOINTS from '../../utils/apiEndPoints';
import LoadingComponent from '../../components/LoadingComponent';
import {getUserLoginState, userDetails} from '../../utils/helperFunctions';
import {useTheme} from '@react-navigation/native';

const DeliveryAddressSetScreen = ({
  route,
  children,
  style,
  navigation,
  ...rest
}) => {
  const [addressType, setAddressType] = useState();
  const [adddress, setAddress] = useState({AddressType: ''});
  const [isloader, setIsloader] = useState(false);
  const [auth, setAuth] = useState();
  const [userData, setUserData] = useState('');
  const theme = useTheme();

  useFocusEffect(
    React.useCallback(() => {
      userLoggedIn();
      // return () => setAddress('')
    }, [auth]),
  );

  const userLoggedIn = async () => {
    // if (await getUserLoginState()) {
    //   setAuth(true)
    // } else {
    //   setAuth(false)
    // }
    setIsloader(false);
    setAuth(true);
    if (await userDetails()) {
      setUserData(await userDetails());
    }
    console.log('global.addressEdit', global.addressEdit);
    if (route.params.type == 'Edit') {
      setAddress({
        ...adddress,
        Name: global.addressEdit ? global.addressEdit.name : '',
        MobileNo: global.addressEdit ? global.addressEdit.mobileNo : '',
        House: global.addressEdit ? global.addressEdit.house : '',
        Area: global.addressEdit ? global.addressEdit.area : '',
        LandMark: global.addressEdit ? global.addressEdit.landMark : '',
        City: global.addressEdit ? global.addressEdit.city : '',
        State: global.addressEdit ? global.addressEdit.state : '',
        PinCode: global.addressEdit ? global.addressEdit.pinCode : '',
        AddressType: global.addressEdit ? global.addressEdit.addressType : '',
        DefaultAddress: global.addressEdit
          ? global.addressEdit.defaultAddress
          : '',
      });
    } else {
      setAddress({AddressType: ''});
    }
  };

  async function onAddDevileryAddress() {
    // if (adddress.Name && adddress.MobileNo && adddress.House && adddress.City && adddress.PinCode) {
    //   setIsloader(true)
    //   var header = {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   }
    //   adddress.CustomerId = userData.id
    //   const data = await apiCall
    //     ('POST', ENDPOINTS.AddCustomerDeliveryAddress, adddress, header);
    //   if (data.data.statusCode === 200) {
    //     setIsloader(false)
    //     Toast.show(data.data.message)
    //     navigation.navigate("DeliveryAddressShow")
    //   } else {
    //     Toast.show(data.data.message)
    //     setIsloader(false)
    //   }
    // } else {
    //   Toast.show('All filed are required')
    // }
  }

  async function EditDevileryAddress() {
    // if (adddress.Name && adddress.MobileNo && adddress.House && adddress.City && adddress.PinCode) {
    //   setIsloader(true)
    //   var header = {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   }
    //   adddress.CustomerId = userData.id
    //   adddress.AddressID = global.addressEdit ? global.addressEdit.addressID : ''
    //   console.log('adddress', adddress)
    //   const data = await apiCall
    //     ('PUT', ENDPOINTS.UpdateDeliveryAddressByID, adddress, header);
    //   console.log('sachin 123', data)
    //   if (data.data.statusCode === 200) {
    //     setIsloader(false)
    //     Toast.show(data.data.message)
    //     navigation.navigate("DeliveryAddressShow")
    //   } else {
    //     Toast.show(data.data.message)
    //     setIsloader(false)
    //   }
    // } else {
    //   Toast.show('All filed are required')
    // }
  }

  return (
    <SafeAreaView style={DEFAULT_STYLES.container}>
      <View style={DEFAULT_STYLES.container}>
        {/* <Header title={route ? route.params.type == 'Edit' ? 'Edit Delivery Address' : 'Delivery Address' : 'Delivery Address'} /> */}
        <View style={{flex: 1}}>
          {isloader && <LoadingComponent />}
          <ScrollView>
            {/* <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 70,
              }}>
              {route ? (
                route.params.type == 'Edit' ? (
                  <Text style={{fontWeight: 'bold', fontSize: 20}}>
                    Edit Delivery Address
                  </Text>
                ) : (
                  <Text style={{fontWeight: 'bold', fontSize: 20}}>
                    Add New Delivery Address
                  </Text>
                )
              ) : (
                <Text style={{fontWeight: 'bold', fontSize: 20}}>
                  Add New Delivery Address
                </Text>
              )}
            </View> */}
            <View style={{marginTop: 10}}>
              <View style={styles.inputView}>
                <TextInput
                  placeholder="Full Name"
                  onChangeText={val =>
                    setAddress({
                      ...adddress,
                      Name: val,
                    })
                  }
                  value={adddress.Name}
                  style={{
                    fontSize: 15,
                  }}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  placeholder="Mobile Number (10 digit[0-9])"
                  onChangeText={val => {
                    let num = val.replace('.', '');
                    if (isNaN(num)) {
                      Toast.show('Enter valid number');
                    } else {
                      setAddress({
                        ...adddress,
                        MobileNo: parseInt(val),
                      });
                    }
                  }}
                  value={adddress.MobileNo}
                  maxLength={10}
                  keyboardType="phone-pad"
                  style={{fontSize: 15}}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  placeholder="Flat,House No, Building,Company,Apartment"
                  onChangeText={val =>
                    setAddress({
                      ...adddress,
                      House: val,
                    })
                  }
                  value={adddress.House}
                  style={{fontSize: 15}}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  placeholder="Area,Colony,Street,Sectore,Village"
                  onChangeText={val =>
                    setAddress({
                      ...adddress,
                      Area: val,
                    })
                  }
                  value={adddress.Area}
                  style={{fontSize: 15}}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  placeholder="Landmark"
                  onChangeText={val =>
                    setAddress({
                      ...adddress,
                      LandMark: val,
                    })
                  }
                  value={adddress.LandMark}
                  style={{fontSize: 15}}
                />
              </View>
              <View style={styles.inputView}>
                <Picker
                  style={{height: 50, width: '100%'}}
                  onValueChange={val =>
                    setAddress({
                      ...adddress,
                      City: val,
                    })
                  }
                  selectedValue={adddress.City}>
                  <Picker.Item label="City" value="" />
                  <Picker.Item label="Bhubaneswar" value="Bhubaneswar" />
                </Picker>
              </View>
              <View style={styles.inputView}>
                <Picker
                  style={{height: 50, width: '100%'}}
                  onValueChange={val =>
                    setAddress({
                      ...adddress,
                      State: val,
                    })
                  }
                  selectedValue={adddress.State}>
                  <Picker.Item label="State" value="" />
                  <Picker.Item label="Odisha" value="Odisha" />
                </Picker>
              </View>
              <View style={styles.inputView}>
                <TextInput
                  placeholder="Pin Code(6 digit[0-9])"
                  keyboardType="number-pad"
                  onChangeText={val =>
                    setAddress({
                      ...adddress,
                      PinCode: parseInt(val),
                    })
                  }
                  value={adddress.PinCode}
                  style={{fontSize: 15}}
                />
              </View>
              <View style={styles.inputView}>
                <Picker
                  style={{height: 40, width: '100%'}}
                  onValueChange={val =>
                    setAddress({
                      ...adddress,
                      AddressType: val,
                    })
                  }
                  selectedValue={adddress.AddressType}>
                  <Picker.Item label="Address Type" value="" />
                  <Picker.Item label="Home" value="Home" />
                  <Picker.Item label="Office" value="Office" />
                </Picker>
                {/* <TextInput placeholder='Address Type' style={{ fontSize: 15 }} /> */}
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  marginLeft: 5,
                  alignItems: 'center',
                }}>
                <View>
                  <CheckBox
                    value={adddress.DefaultAddress}
                    onValueChange={val =>
                      setAddress({
                        ...adddress,
                        DefaultAddress: val,
                      })
                    }
                    style={styles.checkbox}
                    tintColors={{true: APP_THEME}}
                  />
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: 15,
                      color: theme.dark ? 'white' : 'black',
                    }}>
                    Set this as my default delivery address
                  </Text>
                </View>
              </View>
              {route ? (
                route.params.type == 'Edit' ? (
                  <TouchableOpacity
                    onPress={() => EditDevileryAddress()}
                    style={{
                      height: 50,
                      backgroundColor: APP_THEME,
                      margin: 12,
                      borderRadius: 8,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>
                      Edit Delivery Address
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    onPress={() => onAddDevileryAddress()}
                    style={{
                      height: 50,
                      backgroundColor: APP_THEME,
                      margin: 12,
                      borderRadius: 8,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>
                      Add Delivery Address
                    </Text>
                  </TouchableOpacity>
                )
              ) : (
                <TouchableOpacity
                  onPress={() => onAddDevileryAddress()}
                  style={{
                    height: 50,
                    backgroundColor: APP_THEME,
                    margin: 12,
                    borderRadius: 8,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: 'bold', fontSize: 18, color: '#fff'}}>
                    Add Delivery Address
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </ScrollView>
        </View>
        {/* <Footer /> */}
      </View>
    </SafeAreaView>
  );
};

export default DeliveryAddressSetScreen;

var styles = StyleSheet.create({
  headerStyle: {
    width: '100%',
    height: '11%',
    backgroundColor: WHITE,
  },
  tabStyle: {
    flexDirection: 'row',
    backgroundColor: WHITE,
    elevation: -100,
    shadowOpacity: 0,
  },
  tabTextStyle: {
    color: TEXT_TITLE,
    fontWeight: 'bold',
  },
  inputView: {
    marginLeft: 12,
    marginRight: 12,
    paddingLeft: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    backgroundColor: 'white',
    marginBottom: 10,
  },
});
