import React from 'react';
import {
  View,
  Text,
  FlatList,
  Button,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {product} from '../../model/Category';
import Cart from '../../components/Cart';

import LinearGradient from 'react-native-linear-gradient';

const CartScreen = ({navigation}) => {
  const theme = useTheme();

  const renderItem = ({item}) => {
    return (
      <Cart
        itemData={item}
        onPress={() => navigation.navigate('CardItemDetails', {itemData: item})}
      />
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView style={styles.container}>
        <StatusBar barStyle={theme.dark ? 'light-content' : 'dark-content'} />
        <View style={{flexDirection: 'row', paddingBottom: 8}}>
          <View style={{flex: 3, flexWrap: 'wrap', marginLeft: 15}}>
            <Text
              style={{
                color: theme.dark ? 'white' : 'black',
                fontWeight: 'bold',
              }}>
              Delivery Address
            </Text>
            <Text style={{color: theme.dark ? 'white' : 'black'}}>
              Guradiya Kalan
            </Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.change}
              onPress={() => navigation.navigate('AddressScreen')}>
              <LinearGradient
                colors={['#FFA07A', '#FF6347']}
                style={styles.change}>
                <Text
                  style={[
                    styles.textChange,
                    {
                      color: '#fff',
                    },
                  ]}>
                  Change
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            height: 35,
            width: '100%',
            backgroundColor: '#FFA07A',
            shadowColor: '#999',
            shadowOffset: {width: 0, height: 1},
            shadowOpacity: 0.8,
            shadowRadius: 2,
            elevation: 5,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 4,
                flexWrap: 'wrap',
                marginLeft: 15,
                marginTop: 7,
                justifyContent: 'center',
              }}>
              <Text>Shipment 1 of 1</Text>
            </View>
            <View
              style={{
                flex: 1,
                flexWrap: 'wrap',
                marginTop: 7,
                justifyContent: 'center',
              }}>
              <Text>3 item(s)</Text>
            </View>
          </View>
        </View>
        <View>
          <FlatList
            data={product}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
          <View
            style={{
              height: 140,
              width: '95%',
              flexDirection: 'column',
              marginLeft: 10,
              marginVertical: 10,
              shadowColor: '#999',
              borderRadius: 4,
              backgroundColor: '#FFF',
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 3,
                  flexWrap: 'wrap',
                  marginLeft: 15,
                  marginTop: 7,
                }}>
                <Text style={styles.textBold}>Payment Details</Text>
              </View>
              <View
                style={{
                  flex: 1,
                }}>
                <Text />
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 3,
                  flexWrap: 'wrap',
                  marginLeft: 15,
                  marginTop: 7,
                }}>
                <Text>Mrp Totals</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexWrap: 'wrap',
                  marginTop: 7,
                }}>
                <Text> 1324.00</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 3,
                  flexWrap: 'wrap',
                  marginLeft: 15,
                  marginTop: 7,
                }}>
                <Text>Product Discount</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexWrap: 'wrap',
                  marginTop: 7,
                }}>
                <Text> 214.00</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 3,
                  flexWrap: 'wrap',
                  marginLeft: 15,
                  marginTop: 7,
                }}>
                <Text style={styles.textBold}>Total Amount</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexWrap: 'wrap',
                  marginTop: 7,
                }}>
                <Text style={styles.textBold}> 1110.00</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 2,
                  flexWrap: 'wrap',
                  marginLeft: 15,
                  marginTop: 7,
                }}>
                <Text />
              </View>
              <View
                style={{
                  flex: 1,
                  flexWrap: 'wrap',
                  marginTop: 7,
                }}>
                <Text> You save 214.00</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          height: 50,
          width: '100%',
          marginVertical: 10,
          marginBottom: 10,
          shadowColor: '#999',
          backgroundColor: '#FF6',
          borderRadius: 4,
        }}>
        <View style={{flexDirection: 'row', paddingBottom: 8}}>
          <View style={{flex: 2, marginLeft: 15, marginTop: 4}}>
            <Text>Payable Amount</Text>
            <Text>1110.00</Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity style={styles.change}>
              <LinearGradient
                colors={['#FFA07A', '#FF6347']}
                style={styles.change}>
                <Text
                  style={[
                    styles.textChange,
                    {
                      color: '#fff',
                    },
                  ]}>
                  Place Order
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignSelf: 'center',
  },
  button: {
    flex: 1,
    marginTop: 10,
  },
  change: {
    width: '90%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  textChange: {
    fontSize: 13,
  },
  textBold: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
