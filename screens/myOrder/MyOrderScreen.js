import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  AppState,
  FlatList,
  Dimensions,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {DEFAULT_STYLES} from '../../utils/styles';
import {
  WHITE,
  TEXT_SUBTITLE,
  BLACK,
  GREEN,
  TEXT_TITLE,
  APP_THEME,
  GRAY,
} from '../../utils/colors';

import {SafeAreaView} from 'react-native-safe-area-context';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
// import Header from '../../components/Header'
// import Footer from '../../components/Footer'
import _Divider from '../../components/_Divider';
import Text from '../../components/_Text';

import LinearGradient from 'react-native-linear-gradient';

import {
  getUserLoginState,
  userDetails,
  getDateTimeInFormat,
} from '../../utils/helperFunctions';
// import { apiCall } from '../../utils/httpClient';
// import ENDPOINTS from '../../utils/apiEndPoints';
import {useFocusEffect} from '@react-navigation/native';
import LoadingComponent from '../../components/LoadingComponent';
import {useTheme} from '@react-navigation/native';

let deviceWidth = Dimensions.get('window').width;
const MyOrderScreen = ({navigation}) => {
  const [auth, setAuth] = useState();
  const [isLoader, setIsLoader] = useState();
  const [userData, setUserData] = useState('');
  //   const [orderList, setOrderList] = useState([]);
  const theme = useTheme();

  const orderList = [
    {
      id: 1,
      orderNumber: 'Order-002',
      orderStartTime: new Date(),
      totalOrderAmount: 1201,
      orderStatus: 'Successfully Delivered',
      status: 'In Process',
      items:
        'Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2',
      itemCount: 10,
      orderDate: 'May 25, 2022 at 5:00 PM',
    },
    {
      id: 5,
      orderNumber: 'Order-003',
      orderStartTime: new Date(),
      totalOrderAmount: 1201,
      orderStatus: 'Successfully Delivered',
      status: 'Delivered',
      items:
        'Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2',
      itemCount: 10,
      orderDate: 'May 25, 2022 at 5:00 PM',
    },
    {
      id: 2,
      orderNumber: 'Order-002',
      orderStartTime: new Date(),
      totalOrderAmount: 1201,
      orderStatus: 'Successfully Delivered',
      status: 'In Process',
      items:
        'Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2',
      itemCount: 10,
      orderDate: 'May 25, 2022 at 5:00 PM',
    },

    {
      id: 3,
      orderNumber: 'Order-003',
      orderStartTime: new Date(),
      totalOrderAmount: 1201,
      orderStatus: 'Successfully Delivered',
      status: 'Delivered',
      items:
        'Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2',
      itemCount: 10,
      orderDate: 'May 25, 2022 at 5:00 PM',
    },
    {
      id: 4,
      orderNumber: 'Order-004',
      orderStartTime: new Date(),
      totalOrderAmount: 1201,
      orderStatus: 'Successfully Delivered',
      status: 'In Process',
      items:
        'Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2, Soap X 2',
      itemCount: 10,
      orderDate: 'May 25, 2022 at 5:00 PM',
    },
  ];

  const userLoggedIn = async () => {
    // if (await getUserLoginState()) {
    setAuth(true);
    // } else {
    //   setAuth(false);
    // }
    // if (await userDetails()) {
    //   setUserData(await userDetails());
    //   getOrderList(await userDetails());
    // } else {
    //   getOrderList(await userDetails());
    // }
  };
  useFocusEffect(
    React.useCallback(() => {
      //   userLoggedIn();
      //   return () => getOrderList([]);
    }, []),
  );

  const getOrderList = async userData => {
    // var userId = userData ? userData.id : 0;
    // setIsLoader(true);
    // const data = await apiCall(
    //   'GET',
    //   ENDPOINTS.GetAllOrderDetailsByCustomerID + '/' + userId,
    // );
    // if (data.status === 200) {
    //   if (data.data.statusCode === 200) {
    //     setOrderList(data.data.data);
    //     setIsLoader(false);
    //   } else {
    //     setIsLoader(false);
    //   }
    // } else {
    //   setIsLoader(false);
    // }
  };
  function navToOrderDetail(item) {
    // global.orderItem = item;
    // navigation.navigate('DeliveryOption', {orderItem: item});
  }

  return (
    <View style={styles.container}>
      <ScrollView style={styles.container}>
        <StatusBar barStyle={theme.dark ? 'light-content' : 'dark-content'} />
        <View>
          <FlatList
            data={orderList}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1,
                  marginTop: 140,
                }}>
                <Text style={{fontSize: 18}}>Not added any items yet</Text>
              </View>
            }
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => navigation.navigate('MyOrderDetailScreen')}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      height: 175,
                      marginVertical: 10,
                      shadowColor: '#999',
                      shadowOffset: {width: 0, height: 1},
                      shadowOpacity: 0.8,
                      shadowRadius: 2,
                      elevation: 5,
                      borderRadius: 8,
                      width: '95%',
                      backgroundColor: '#FFFFFF',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          flex: 1,
                          flexWrap: 'wrap',
                          marginLeft: 15,
                          marginTop: 7,
                          justifyContent: 'center',
                          alignItems: 'center',
                          fontWeight: 'bold',
                        }}>
                        <Text
                          style={{
                            fontWeight: 'bold',
                          }}>
                          {item.orderNumber}
                        </Text>
                      </View>
                      {/* <View
                    style={{
                      flex: 1,
                      flexWrap: 'wrap',
                      marginTop: 7,
                      justifyContent: 'center',
                    }}>
                    <Text>3 item(s)</Text>
                  </View> */}
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{
                          flexWrap: 'wrap',
                          marginLeft: 15,
                          // marginTop: 7,
                        }}>
                        <Text
                          style={{
                            fontWeight: 'bold',
                          }}>
                          {item.orderDate}
                        </Text>
                      </View>
                      {/* <View
                      style={{
                        flexWrap: 'wrap',
                        marginTop: 7,
                        marginRight: 20,
                      }}>
                      <Text>1131</Text>
                    </View> */}
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{
                          flexWrap: 'wrap',
                          marginLeft: 15,
                          marginTop: 9,
                        }}>
                        <Text
                          style={{
                            fontWeight: 'bold',
                          }}>
                          {item.itemCount} Items
                        </Text>
                      </View>
                      <View
                        style={{
                          flexWrap: 'wrap',
                          marginTop: 9,
                          marginRight: 20,
                        }}>
                        <Text
                          style={{
                            fontWeight: 'bold',
                          }}>
                          {item.totalOrderAmount}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        width: '90%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        description
                        style={{
                          // flexWrap: 'wrap',
                          marginLeft: 15,
                        }}
                        numberOfLines={2}>
                        <Text>{item.items}</Text>
                      </View>
                    </View>

                    <View
                      style={{
                        margin: 15,
                        marginBottom: 0,
                        // marginBottom: 7,
                        // borderBottomWidth: 1,
                        borderBottomWidth: 0.5,
                        borderBottomColor: GRAY,
                      }}
                    />
                    {item.status == 'Delivered' ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          style={{
                            flexWrap: 'wrap',
                            marginLeft: 15,
                            marginTop: 7,
                          }}>
                          <Text
                            style={{
                              color: '#15edac',
                              fontWeight: 'bold',
                            }}>
                            Delivered
                          </Text>
                        </View>
                        <View
                          style={{
                            flexWrap: 'wrap',
                            marginTop: 7,
                            marginRight: 20,
                          }}>
                          <Text
                            style={{
                              color: '#e3072b',
                              fontWeight: 'bold',
                            }}>
                            Re-order
                          </Text>
                        </View>
                      </View>
                    ) : (
                      <View
                        style={{
                          justifyContent: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          style={{
                            height: 30,
                            width: '25%',
                            // marginLeft: 10,
                            // marginRight: 10,
                            marginVertical: 10,
                            marginBottom: 5,
                            marginLeft: 15,
                            shadowColor: '#999',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#FFFFFF',
                            borderRadius: 4,
                            borderWidth: 1,
                            borderColor: APP_THEME,
                          }}>
                          <View style={{flexDirection: 'row'}}>
                            <View style={styles.button}>
                              <TouchableOpacity
                                style={styles.change}
                                onPress={() =>
                                  navigation.navigate('MyOrderDetailScreen')
                                }>
                                <Text
                                  style={[
                                    styles.textChange,
                                    {
                                      color: APP_THEME,
                                    },
                                  ]}>
                                  Track Order >
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      </View>
                    )}
                  </View>
                </View>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id.toString()}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default MyOrderScreen;

var styles = StyleSheet.create({
  headerStyle: {
    width: '100%',
    height: '11%',
    backgroundColor: WHITE,
  },
  button: {
    flex: 1,
  },
  change: {
    width: '90%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  textChange: {
    fontSize: 13,
  },
  textBold: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    width: '100%',
    alignSelf: 'center',
  },
  squareBox: {
    height: 160,
    width: '100%',
    // marginLeft: 10,
    // marginVertical: 10,
    // flexDirection: 'row',
    shadowColor: '#ffffff',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    // borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // borderColor: APP_THEME,
    padding: 10,
    marginTop: 10,
  },

  tabStyle: {
    flexDirection: 'row',
    backgroundColor: WHITE,
    elevation: -100,
    shadowOpacity: 0,
  },
  tabTextStyle: {
    color: TEXT_TITLE,
    fontWeight: 'bold',
  },
});
