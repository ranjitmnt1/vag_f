import React, {useRef} from 'react';
import {View, Button, Image, ScrollView, StyleSheet} from 'react-native';
import {subCategry} from '../model/Category';

import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Text, TouchableRipple} from 'react-native-paper';

const SubCategoryScreen = ({route, navigation}) => {
  const itemData = route.params.itemData;
  const theme = useTheme();
  const initialMapState = {
    searchData: subCategry,
  };

  const [state, setData] = React.useState(initialMapState);
  const {colors} = useTheme();
  return (
    <View style={styles.resultContainer}>
      <ScrollView>
        {state.searchData.map((item, index) => (
          <TouchableRipple
            key={index}
            onPress={() =>
              navigation.navigate('ProductListScreen', {itemData: item})
            }>
            <View style={styles.menuItem}>
              <Image style={styles.menuItemImg} source={item.image} />
              <Text style={styles.menuItemText}>{item.title}</Text>
              <Icon
                style={styles.manuIcon}
                name="ios-arrow-dropright"
                color={colors.text}
                size={23}
              />
            </View>
          </TouchableRipple>
        ))}
      </ScrollView>
    </View>
  );
};

export default SubCategoryScreen;

const styles = StyleSheet.create({
  resultContainer: {
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },

  container: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  manuIcon: {
    alignSelf: 'center',
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: 16,
    lineHeight: 26,
  },
  menuItemImg: {
    width: 35,
    height: 35,
    alignSelf: 'center',
  },
});
