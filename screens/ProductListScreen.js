import React from 'react';
import {View, Text, Button, FlatList, StyleSheet} from 'react-native';
import {product} from '../model/Category';
import Product from '../components/Product';
import {useTheme} from '@react-navigation/native';

// const initialMapState = {
//   productData: product,
// };

// const [state, setData] = React.useState(initialMapState);

const ProductListScreen = ({route, navigation}) => {
  const {colors} = useTheme();

  const renderItem = ({item}) => {
    return (
      <Product
        itemData={item}
        onPress={() => navigation.navigate('CardItemDetails', {itemData: item})}
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={product}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

export default ProductListScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignSelf: 'center',
  },
});
