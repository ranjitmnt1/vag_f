import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  AppState,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {DEFAULT_STYLES} from '../../utils/styles';
import {
  WHITE,
  TEXT_SUBTITLE,
  BLACK,
  GREEN,
  TEXT_TITLE,
  APP_THEME,
  GRAY,
  MENU_GRAY,
  LIGHT_GRAY,
} from '../../utils/colors';

import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView} from 'react-native-gesture-handler';
import {useFocusEffect} from '@react-navigation/native';
// import Feather from 'react-native-vector-icons/dist/Feather';
// import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
// import AntDesign from 'react-native-vector-icons/dist/AntDesign';
// import Entypo from 'react-native-vector-icons/dist/Entypo';
import StepIndicator from 'react-native-step-indicator';
// import _Divider from '../../components/_Divider';
import Text from '../../components/_Text';
// import {getUserLoginState, userDetails} from '../../utils/helperFunctions';
// import {apiCall} from '../../utils/httpClient';
// import ENDPOINTS from '../../utils/apiEndPoints';
// import LoadingComponent from '../../components/LoadingComponent';
let deviceWidth = Dimensions.get('window').width;

const MyOrderDetailScreen = ({navigation}) => {
  const [currentPosition, setCurrentPosition] = useState(1);
  const [auth, setAuth] = useState();
  const [isLoader, setIsLoader] = useState();
  const [userData, setUserData] = useState('');
  // const [orderList, setOrderList] = useState([]);

  const orderList = [
    {
      id: 1,
      productName: 'Tea',
      unit: '100gm',
      quantity: 2,
      salePrice: 10,
      cost: 20,
      totalAmt: 100,
    },
    {
      id: 2,
      productName: 'Coffee',
      unit: '100gm',
      quantity: 2,
      salePrice: 10,
      cost: 20,
      totalAmt: 100,
    },
    {
      id: 3,
      productName: 'Milk',
      unit: '100gm',
      quantity: 2,
      salePrice: 10,
      cost: 20,
      totalAmt: 100,
    },
    {
      id: 4,
      productName: 'Ghee',
      unit: '100gm',
      quantity: 2,
      salePrice: 10,
      cost: 20,
      totalAmt: 100,
    },
    {
      id: 5,
      productName: 'Tea',
      unit: '100gm',
      quantity: 2,
      salePrice: 10,
      cost: 20,
      totalAmt: 100,
    },
  ];

  const labels = ['', 'Order Created'];
  const stepIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: APP_THEME,
    separatorFinishedColor: APP_THEME,
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: APP_THEME,
    stepIndicatorUnFinishedColor: '#aaaaaa',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 12,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
    labelColor: '#666666',
    labelSize: 15,
    currentStepLabelColor: APP_THEME,
  };
  const userLoggedIn = async () => {
    // if (await getUserLoginState()) {
    setAuth(true);
    // } else {
    //   setAuth(false)
    // }
    // if (await userDetails()) {
    //   setUserData(await userDetails())
    //   getOrderList(await userDetails())
    // } else {
    //   getOrderList(await userDetails())
    // }
  };
  useFocusEffect(
    React.useCallback(() => {
      userLoggedIn();
      return () => getOrderList([]);
    }, []),
  );

  const getOrderList = async userData => {
    // var userId = userData ? userData.id : 0
    // setIsLoader(true)
    // const data = await apiCall
    //   ('GET', ENDPOINTS.GetOrderDetailsByOrderID + '/' + global.orderItem.parentOrderID + '/' + userId);
    // if (data.status === 200) {
    //   if (data.data.statusCode === 200) {
    //     setOrderList(data.data.data.customerOrderDetailsList)
    //     setIsLoader(false)
    //   } else {
    //     setIsLoader(false)
    //   }
    // } else {
    //   setIsLoader(false)
    // }
  };

  return (
    <SafeAreaView style={DEFAULT_STYLES.container}>
      <View style={DEFAULT_STYLES.container}>
        <ScrollView
          contentContainerStyle={{marginTop: 5, flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                minHeight: 175,
                // borderBottomWidth: 1,
                // borderBottomColor: '#eaeaea',
                // marginHorizontal: 10,

                // height: 175,
                marginVertical: 10,
                shadowColor: '#999',
                shadowOffset: {width: 0, height: 1},
                shadowOpacity: 0.8,
                shadowRadius: 2,
                elevation: 5,
                borderRadius: 8,
                width: '95%',
                backgroundColor: '#FFFFFF',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                      fontSize: 13,
                      // fontWeight: 'bold',
                    }}>
                    placed on fri, 6 mar, 11:50 am
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 6,
                  }}>
                  <Text
                    style={{
                      color: APP_THEME,
                      fontWeight: 'bold',
                      fontSize: 18,
                      // fontWeight: 'bold',
                    }}>
                    yay! order successfully delivered
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 6,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                      fontSize: 13,
                      // fontWeight: 'bold',
                    }}>
                    delivered on
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                  }}>
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: 'bold',
                    }}>
                    sat, 7 mar 6:21 pm
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      // fontSize: 11,
                      fontWeight: 'bold',
                    }}>
                    15 item, Order id: ORD1234-001
                  </Text>
                </View>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: 30,
                    width: '30%',
                    // marginLeft: 10,
                    // marginRight: 10,
                    marginVertical: 10,
                    marginBottom: 15,
                    marginLeft: 15,
                    shadowColor: '#999',
                    backgroundColor: '#FFFFFF',
                    borderRadius: 4,
                    borderWidth: 1,
                    borderColor: '#FF6347',
                  }}>
                  <View>
                    <View style={styles.button}>
                      <TouchableOpacity
                        style={styles.change}
                        onPress={() =>
                          navigation.navigate('MyOrderDetailScreen')
                        }>
                        <Text
                          style={[
                            styles.textChange,
                            {
                              color: '#FF6347',
                            },
                          ]}>
                          Invoice
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    height: 30,
                    width: '30%',
                    // marginLeft: 10,
                    // marginRight: 10,
                    marginVertical: 10,
                    // marginBottom: 5,
                    marginLeft: 15,
                    shadowColor: '#999',
                    backgroundColor: '#FFFFFF',
                    borderRadius: 4,
                    marginRight: 15,
                    borderWidth: 1,
                    borderColor: '#FF6347',
                  }}>
                  <View>
                    <View style={styles.button}>
                      <TouchableOpacity
                        style={styles.change}
                        onPress={() =>
                          navigation.navigate('MyOrderDetailScreen')
                        }>
                        <Text
                          style={[
                            styles.textChange,
                            {
                              color: '#FF6347',
                            },
                          ]}>
                          Re-order
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>

            <View
              style={{
                minHeight: 175,
                // borderBottomWidth: 1,
                // borderBottomColor: '#eaeaea',
                // marginHorizontal: 10,

                // height: 175,
                marginVertical: 10,
                shadowColor: '#999',
                shadowOffset: {width: 0, height: 1},
                shadowOpacity: 0.8,
                shadowRadius: 2,
                elevation: 5,
                borderRadius: 8,
                width: '95%',
                backgroundColor: '#FFFFFF',
              }}>
              {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                    }}>
                    Order-002
                  </Text>
                </View>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginTop: 9,
                    marginRight: 20,
                  }}>
                  <Text
                    style={{
                      color: APP_THEME,
                      fontWeight: 'bold',
                    }}>
                    Delivered
                  </Text>
                </View>
              </View> */}

              {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    // marginTop: 9,
                  }}>
                  <Text
                    style={{
                      // color: GRAY,
                      fontWeight: 'bold',
                    }}>
                    27 May, 2020
                  </Text>
                </View>
              </View> */}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 10,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                      fontWeight: 'bold',
                    }}>
                    Delivered To
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                    }}>
                    270, Guradiya Kalan
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 10,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                      fontWeight: 'bold',
                    }}>
                    Payment Method
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                    }}>
                    Cash
                  </Text>
                </View>
              </View>

              <View
                style={{
                  margin: 15,
                  marginBottom: 0,
                  // marginBottom: 7,
                  // borderBottomWidth: 1,
                  borderBottomWidth: 0.5,
                  borderBottomColor: GRAY,
                }}
              />

              <View>
                <FlatList
                  data={orderList}
                  showsVerticalScrollIndicator={false}
                  ListEmptyComponent={
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                        marginTop: 140,
                      }}>
                      <Text style={{fontSize: 18}}>
                        Not added any items yet
                      </Text>
                    </View>
                  }
                  renderItem={({item}) => (
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          style={{
                            flexWrap: 'wrap',
                            marginLeft: 15,
                            marginTop: 9,
                          }}>
                          <Text
                            style={{
                              fontWeight: 'bold',
                            }}>
                            {item.productName} X {item.quantity}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexWrap: 'wrap',
                            marginTop: 9,
                            marginRight: 20,
                          }}>
                          <Text
                            style={{
                              color: GRAY,
                              fontWeight: 'bold',
                            }}>
                            {item.totalAmt}
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          style={{
                            flexWrap: 'wrap',
                            marginLeft: 15,
                            marginTop: 9,
                          }}>
                          <Text
                            style={{
                              color: GRAY,
                              fontWeight: 'bold',
                            }}>
                            {item.unit}
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          margin: 15,
                          marginBottom: 0,
                          // marginBottom: 7,
                          // borderBottomWidth: 1,
                          borderBottomWidth: 0.5,
                          borderBottomColor: GRAY,
                        }}
                      />
                    </View>
                  )}
                  keyExtractor={item => item.id.toString()}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginLeft: 15,
                      marginTop: 9,
                    }}>
                    <Text
                      style={{
                        color: GRAY,
                      }}>
                      Payment Summary
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginLeft: 15,
                      marginTop: 9,
                    }}>
                    <Text
                      style={{
                        color: GRAY,
                      }}>
                      M.R.P.
                    </Text>
                  </View>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginTop: 9,
                      marginRight: 20,
                    }}>
                    <Text
                      style={{
                        color: APP_THEME,
                        // fontWeight: 'bold',
                      }}>
                      $1646
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginLeft: 15,
                      marginTop: 9,
                    }}>
                    <Text
                      style={{
                        color: GRAY,
                      }}>
                      Product discount
                    </Text>
                  </View>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginTop: 9,
                      marginRight: 20,
                    }}>
                    <Text
                      style={{
                        color: APP_THEME,
                        // fontWeight: 'bold',
                      }}>
                      -$146
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginLeft: 15,
                      marginTop: 9,
                    }}>
                    <Text
                      style={{
                        color: GRAY,
                      }}>
                      Wallet amount
                    </Text>
                  </View>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginTop: 9,
                      marginRight: 20,
                    }}>
                    <Text
                      style={{
                        color: APP_THEME,
                        // fontWeight: 'bold',
                      }}>
                      -$95
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginLeft: 15,
                      marginTop: 9,
                    }}>
                    <Text
                      style={{
                        color: GRAY,
                      }}>
                      Delivery charges
                    </Text>
                  </View>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginTop: 9,
                      marginRight: 20,
                    }}>
                    <Text
                      style={{
                        color: APP_THEME,
                        // fontWeight: 'bold',
                      }}>
                      free
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    margin: 15,
                    marginBottom: 0,
                    // marginBottom: 7,
                    // borderBottomWidth: 1,
                    borderBottomWidth: 0.5,
                    borderBottomColor: GRAY,
                  }}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginLeft: 15,
                      marginTop: 9,
                      marginBottom: 9,
                    }}>
                    <Text
                      style={{
                        // color: GRAY,
                        fontWeight: 'bold',
                      }}>
                      Final paid amount
                    </Text>
                  </View>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      marginTop: 9,
                      marginRight: 20,
                    }}>
                    <Text
                      style={{
                        // color: APP_THEME,
                        fontWeight: 'bold',
                      }}>
                      $1133
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View
              style={{
                minHeight: 160,
                marginTop: 10,
                // borderBottomWidth: 1,
                // borderBottomColor: '#eaeaea',
                // marginHorizontal: 10,

                // height: 175,
                marginVertical: 10,
                shadowColor: '#999',
                shadowOffset: {width: 0, height: 1},
                shadowOpacity: 0.8,
                shadowRadius: 2,
                elevation: 5,
                borderRadius: 8,
                width: '95%',
                backgroundColor: '#FFFFFF',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      // color: GRAY,
                      fontWeight: 'bold',
                    }}>
                    Refund amount details
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      color: APP_THEME,
                      fontWeight: 'bold',
                    }}>
                    Cashback amount
                  </Text>
                </View>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginTop: 9,
                    marginRight: 20,
                  }}>
                  <Text
                    style={{
                      color: APP_THEME,
                      fontWeight: 'bold',
                    }}>
                    $50
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                    }}>
                    Cashback initated date
                  </Text>
                </View>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginTop: 9,
                    marginRight: 20,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                    }}>
                    06 mar 2022
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginLeft: 15,
                    marginTop: 9,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                    }}>
                    Cashback source
                  </Text>
                </View>
                <View
                  style={{
                    flexWrap: 'wrap',
                    marginTop: 9,
                    marginRight: 20,
                  }}>
                  <Text
                    style={{
                      color: GRAY,
                    }}>
                    VAG-F wallet
                  </Text>
                </View>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    marginTop: 10,
                    height: 27,
                    // borderRadius: 8,
                    borderColor: GRAY,
                    borderWidth: 1,
                    width: '94%',
                    backgroundColor: '#faf7f8',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        marginTop: 2,
                        marginLeft: 3,
                        alignContent: 'center',
                      }}>
                      <Text
                        style={{
                          color: GRAY,
                        }}>
                        Cashback of $50.0 credited to your VAG-F wallet.
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default MyOrderDetailScreen;

var styles = StyleSheet.create({
  headerStyle: {
    width: '100%',
    height: '11%',
    backgroundColor: WHITE,
  },
  tabStyle: {
    flexDirection: 'row',
    backgroundColor: WHITE,
    elevation: -100,
    shadowOpacity: 0,
  },
  tabTextStyle: {
    color: TEXT_TITLE,
    fontWeight: 'bold',
  },
  button: {
    flex: 1,
  },
  change: {
    width: '90%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  textChange: {
    fontSize: 13,
  },
  textBold: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
