import React from 'react';
import {
  View,
  Text,
  Button,
  FlatList,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {topCategry} from '../model/Category';
import Category from '../components/Category';

const numColumns = 2;

const CategoryScreen = ({navigation}) => {
  const renderItem = ({item}) => {
    return (
      <Category
        itemData={item}
        onPress={() =>
          navigation.navigate('SubCategoryScreen', {itemData: item})
        }
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={topCategry}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        numColumns={numColumns}
      />
    </View>
  );
};

export default CategoryScreen;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  // container: {
  //   flex: 1,
  //   width: '98%',
  //   alignSelf: 'center',
  //   flexDirection: 'column',
  // },
});
