const Images = [
  {image: require('../assets/profile.png')}
];

export const OurTeam = [
  {
    id: 1,
    name: 'Member First',
    designation: 'Founder',
    image: Images[0].image
  },
  {
    id: 2,
    name: 'Sandeep Patidar',
    designation: 'Co-Founder',
    image: Images[0].image
  },
  {
    id: 3,
    name: 'Member Second',
    designation: 'Developer',
    image: Images[0].image
  },
  {
    id: 4,
    name: 'Member Third',
    designation: 'Developer',
    image: Images[0].image
  }
 
];